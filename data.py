import requests
import json
import logging
from flask import Flask,request
from datetime import date

logging.basicConfig(filename='agecheck.log',level=logging.DEBUG,format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')

app = Flask(__name__)


try:
    
    @app.route("/",methods=["GET","POST"])
    def agecheck():
        try:
            reqst=request.get_json()
            print(reqst)
        except Exception as e:
            logging.error("Exception occurred", exc_info=True)
            
            return "invalid json"
        
        logging.debug(reqst)

        try:
            dob = reqst.get("DOB").split("-")
            if int(dob[0])>31 or int(dob[1])>12 or int(dob[2])<1900 or int(dob[2])>2022:
                logging.debug(dob,"Invalid Date of Birth")
                return("Plese check formate of your date it is DD-MM-YY and also check date,month,year value")
            elif dob == "":
                logging.debug(dob,"Plese Enter Valid Date of birth")
                return("please give Date of Birth") 
            print(dob)
            logging.debug(dob)
        except Exception as e:
            logging.error("Exception occurred", exc_info=True)
            return "Invalid format"
        
        age = age_calculater(date(int(dob[2]),int(dob[1]), int(dob[0])))
        print(age)
        if age> 18:
            activity,status  = get_random()

            if status:
                return {
                    "success" : "True",
                    "activity": activity
                }  
            else :
                return {
                    "success":"False",
                    "message":"couldnt fetch an activity"
                },400     
        else:
            return {
                "success" : "False",
                "error" : "Should be older than 18 years"
            },400
    


    def get_random():

        try:
            url = "http://www.boredapi.com/api/activity"
            print("get random activity")
            payload={}
            response = requests.request("GET", url, data=payload)
            response = json.loads(response.text)
            print(response['activity'])
            return response['activity'],True
        except Exception as e:
            print("some error occured",e)
            return "some error occured",False


    def age_calculater(brthdate):
        year_days = 365
        age = int((date.today() - brthdate).days / year_days)
        return age

except:

    print("Unable to process")



if __name__=="__main__":
    app.run(port=5500,debug=True)    